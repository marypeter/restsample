package com.niit.myrest.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.niit.myrest.model.Person;

@Repository
public interface PersonRepo extends JpaRepository<Person,String>{
	
	public Person findByEmailid(String mailid);

}
