package com.niit.myrest.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.niit.myrest.exception.UserAlreadyExistException;
import com.niit.myrest.exception.UserNotFoundException;
import com.niit.myrest.model.Person;
import com.niit.myrest.repository.PersonRepo;

@Service
public class PersonDAOImpl implements PersonDAO {

	 @Autowired
	 PersonRepo repo;
	
	
	@Override
	public Person addRecord(Person person) throws UserAlreadyExistException {
		Person pobj;
		Optional<Person> personobj=repo.findById(person.getEmailid());
		if(personobj.isPresent())
			throw new UserAlreadyExistException("Emailid already exist");
		else
		{
			 pobj=repo.save(person);
		}
		return pobj;
	}

	@Override
	public List ShowAll() {
		// TODO Auto-generated method stub
		
		List list=repo.findAll();
		 
		return list;
	}

	@Override
	public Person getPerson(String emailid) throws UserNotFoundException 
	{
		Person person=repo.findByEmailid(emailid);
     
		
		
		if(person==null)
			throw new UserNotFoundException("User not found");
		else
			
		 person=repo.findByEmailid(emailid);
		
		return person;
	}

	@Override
	public boolean DelRecord(String emailid) throws UserNotFoundException {
		
	Person person=repo.findByEmailid(emailid);
     
		
		
		if(person==null)
			throw new UserNotFoundException("User not found");
		else
			repo.delete(person);
		
		return false;
	}

	@Override
	public Person updatePerson(Person personobj) throws UserNotFoundException {
Person person=repo.findByEmailid(personobj.getEmailid());
     
		
		
		if(person==null)
			throw new UserNotFoundException("User not found");
		else
		{
		  person.setName(personobj.getName());	
			repo.save(person);
			
		}
		return null;
	}

 

	
}
