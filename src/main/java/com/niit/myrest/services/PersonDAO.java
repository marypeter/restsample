package com.niit.myrest.services;

import java.util.List;

import com.niit.myrest.exception.UserAlreadyExistException;
import com.niit.myrest.exception.UserNotFoundException;
import com.niit.myrest.model.Person;

public interface PersonDAO {

	Person addRecord(Person person) throws UserAlreadyExistException;
	List ShowAll();
	Person getPerson(String emaild) throws UserNotFoundException;
	boolean DelRecord(String emailid) throws UserNotFoundException;
	Person updatePerson(Person person) throws UserNotFoundException;
 }
