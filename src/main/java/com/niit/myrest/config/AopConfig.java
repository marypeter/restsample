package com.niit.myrest.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@Configuration
@EnableAspectJAutoProxy

public class AopConfig {

	@Bean

	public LogAspect Loggmethod()
	{
		return new LogAspect();
	}
	
}
