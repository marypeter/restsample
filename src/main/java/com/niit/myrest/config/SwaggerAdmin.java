package com.niit.myrest.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2

public class SwaggerAdmin {
	
	@Bean
	public Docket generate()
	{
		return new Docket(DocumentationType.SWAGGER_2)
		            .select()
		            .apis(RequestHandlerSelectors.basePackage("com.niit.myrest.controller"))
		            .build()
		            .apiInfo(apimethod()).useDefaultResponseMessages(false);
		
				}

	private ApiInfo apimethod()
	{
		ApiInfoBuilder apiobj=new ApiInfoBuilder();
		
		apiobj.title("CTS FULLTIME BATCH 23").version("12.0.1").license("STACKROUTE.IN").description("Just to demonstrate");
		
		return apiobj.build();
	}
}
