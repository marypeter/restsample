package com.niit.myrest.config;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

 
@Aspect
@Component 

public class LogAspect {
	
	public static final Logger mylogger=LoggerFactory.getLogger(LogAspect.class);
	
	@Before("execution (* com.niit.myrest.controller.MyController.*(..))")
public void logfirst(JoinPoint joinpoint)
{
mylogger.info("Some User Accessing controller methods");
}

	@After("personadd()")
	
	public void aftercall(JoinPoint joinpoints)
	{
		mylogger.info("Person details are getting added"+joinpoints.getSignature());
		
	}
	
	@Around("personadd()")
	public Object getmedetail(ProceedingJoinPoint projp)
	{
		Object object=null;
		try
		{
			object=projp.proceed();
		}
		catch(Throwable e)
		{
			
		}
		mylogger.info(object.toString());
		return object;
	}
	
	
	
	@Before("personadd()")
	public void beforesecond(JoinPoint joinpoint)
	{
	mylogger.info("Going to Add");
	}	

@Pointcut("execution (* com.niit.myrest.controller.MyController.addPerson(..))")	
	public void personadd()
	{
	 
	}

	
}