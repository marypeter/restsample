package com.niit.myrest.exception;

public class UserNotFoundException extends Exception {

	public  UserNotFoundException(String msg)
	 {
		 super(msg);
	 }
}
