package com.niit.myrest.exception;

public class UserAlreadyExistException extends Exception {

	public  UserAlreadyExistException(String mes)
	{
		 super(mes);
	}
}
