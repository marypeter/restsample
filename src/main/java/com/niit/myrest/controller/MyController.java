package com.niit.myrest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.niit.myrest.exception.UserAlreadyExistException;
import com.niit.myrest.exception.UserNotFoundException;
import com.niit.myrest.model.Person;
import com.niit.myrest.services.PersonDAO;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("cts/demo")
public class MyController {
	
	@Autowired
	PersonDAO personservice;
	
	
	
	@GetMapping("/show")
	public ResponseEntity<?> show()
	{
		return new ResponseEntity<String>("Welcome to rest app",HttpStatus.OK);
	}

	@PostMapping("/save")
	public ResponseEntity<?> addPerson(@RequestBody Person person)
	{
		try
		{
			personservice.addRecord(person);
		}
		catch(UserAlreadyExistException e)
		{
			return new ResponseEntity<String>(e.getMessage(),HttpStatus.CONFLICT);
		}
		return new ResponseEntity<String>("Person Added",HttpStatus.OK);
	}
	
	@ApiOperation(value="This method will be used for retrieve. to use Get")
	@RequestMapping ("/view/{empid}")
	
	public ResponseEntity<?> showDetails(@PathVariable("empid") String eid)
	{
	Person person;
		try
		{
			person=personservice.getPerson(eid);
		}
		catch(UserNotFoundException ufe)
		{
			return new ResponseEntity<String>(ufe.getMessage(),HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Person>(person,HttpStatus.OK);
	}
	
	
	
	@ApiOperation(value="This method will show you all record")
	@GetMapping("/showall")
	

	public ResponseEntity<?> showall( )
	{
	 List li=personservice.ShowAll();
		
			return new ResponseEntity<List>(li,HttpStatus.OK);
	
		
			}
	
@RequestMapping("/delete/{empid}")
	
	public ResponseEntity<?> Deleterec(@PathVariable("empid") String eid)
	{
	Person person;
		try
		{
			personservice.DelRecord(eid);
		}
		catch(UserNotFoundException ufe)
		{
			return new ResponseEntity<String>("Not Found",HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<String>("Deleted succes",HttpStatus.OK);
	}

@RequestMapping("/modify")

public ResponseEntity<?> modifyRec(@RequestBody Person personobj)
{
Person person;
	try
	{
		 person=personservice.updatePerson(personobj);
	}
	catch(UserNotFoundException ufe)
	{
		return new ResponseEntity<String>("Not Found",HttpStatus.NOT_FOUND);
	}
	return new ResponseEntity<Person>(person,HttpStatus.OK);
}


}


