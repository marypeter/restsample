package com.niit.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.niit.myrest.exception.UserAlreadyExistException;
import com.niit.myrest.model.Person;
import com.niit.myrest.repository.PersonRepo;
import com.niit.myrest.services.PersonDAOImpl;


public class ServiceTest {

	@Mock
	PersonRepo myrepo;
	
	private Person personobj;
	
	
	@InjectMocks
	PersonDAOImpl persondao;
	
	@Before
	public void initial() throws Exception
	{
		MockitoAnnotations.initMocks(this);
		
		personobj=new Person();
		
		personobj.setEmailid("dan@gmail.com");
		personobj.setName("Dan");
		
		
	}
	
	
	
	@Test
	
	public void savePerson() throws UserAlreadyExistException
	
	{
		Mockito.when(myrepo.save(personobj)).thenReturn(personobj);
		
		Person person=myrepo.save(personobj);
		
		Assert.assertEquals("passed",person,personobj);
	}
	
}
