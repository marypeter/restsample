package com.niit.myrest.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.niit.myrest.controller.MyController;
import com.niit.myrest.model.Person;
import com.niit.myrest.services.PersonDAO;

@RunWith(SpringRunner.class)
@WebMvcTest
public class ControllerTest {

	@Autowired
	private MockMvc mockmvc;
	
	@MockBean
	PersonDAO personservice;
	
	@InjectMocks
	MyController mycontrol;
	
	
	private Person personobj;
	
	@Before
	
	public void setUp() throws Exception
	{
		
		MockitoAnnotations.initMocks(this);
		
		mockmvc=MockMvcBuilders.standaloneSetup(mycontrol).build();
		personobj=new Person();
		personobj.setEmailid("Mary@niit.com");
		personobj.setName("Mary");
			
	}
	
	@Test
	
	public void Addrec() throws Exception
	{
		Mockito.when(personservice.addRecord(personobj)).thenReturn(personobj);
		
		mockmvc.perform(MockMvcRequestBuilders.post("/cts/demo/save")
				.contentType(MediaType.APPLICATION_JSON).content(jsonconvert(personobj)))
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andDo(MockMvcResultHandlers.print());
				
				
				
	}
	
	public   String jsonconvert(Object objinput) throws JsonProcessingException
	{
	String valu=null;
	try
	{
		final ObjectMapper mapobj=new ObjectMapper();
		
		final String jsonout=mapobj.writeValueAsString(objinput);
		valu=jsonout;
	}
	catch(JsonProcessingException e)
	{
		
	}
	return valu;
	
	}
}
