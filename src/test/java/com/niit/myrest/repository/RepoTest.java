package com.niit.myrest.repository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;


import com.niit.myrest.model.Person;
import com.niit.myrest.repository.PersonRepo;

import junit.framework.Assert;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace=AutoConfigureTestDatabase.Replace.NONE)

public class RepoTest {
@Autowired
PersonRepo repo;

private Person person;

@Before 
public void setUp( ) throws Exception
{
	person=new Person();
	person.setEmailid("Anu@gmail.com");
	person.setName("Anu");
}

@Test

public void addPersonDetails()
{
	repo.save(person);
	
	Person pobj=repo.findByEmailid(person.getEmailid());
	
	Assert.assertEquals(pobj.getEmailid(),person.getEmailid());
}


}
